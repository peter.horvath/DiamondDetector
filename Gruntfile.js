module.exports = function(grunt) {
  'use strict';

  require('google-closure-compiler').grunt(grunt);
  // require it at the top and pass in the grunt instance
  //require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      options: {
        jshintrc: 'jshint.json'
      },
      dev: {
        src: ['Gruntfile.js']
      }
    },

    tslint: {
      options: {
        configuration: "tslint.json",
        force: false,
        fix: false
      },
      files: {
        src: ["src/**/*.ts"]
      }
    },

    mkdir: {
      dist: {
        options: {
          create: ['dist/tsc', 'dist/closure']
        }
      }
    },

    tsc: {
      dist: {
        options: [ "-p", "tsconfig.json" ],
        debug: true
      }
    },

    exec: {
      closure: {
        options: {
          command: 'java',
          args: [
            "-jar", "./node_modules/google-closure-compiler/compiler.jar",
            "--flagfile", "./closure.conf"
          ],
          timeout: 60,
          stdout: true,
          stderr: true
        }
      },
      "closure-test": {
        options: {
          command: 'java',
          args: [
            "-jar", "./node_modules/google-closure-compiler/compiler.jar",
            "--flagfile", "./test/closure.conf"
          ],
          timeout: 60,
          stdout: true,
          stderr: true
        }
      }
    },

    comments: {
      dist: {
        options: {
          singeline: true,
          multiline: true
        },
        src: [ "dist/closure/bundle.js" ]
      }
    },

    clean: ['dist']
  });

  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-direct-exec");
  grunt.loadNpmTasks("grunt-direct-tsc");
  grunt.loadNpmTasks("grunt-mkdir");
  grunt.loadNpmTasks("grunt-stripjscomments");
  grunt.loadNpmTasks("grunt-tslint");

  grunt.registerTask("check", ["jshint", "tslint"]);

  grunt.registerTask("dist", ["clean", "jshint", "tslint", "mkdir", "tsc", "exec:closure", "comments"]);

  grunt.registerTask("test", ["clean", "jshint", "mkdir", "exec:closure-test"]);

  grunt.registerTask("default", ["dist"]);
};
